﻿//  Project Name:   SQII_PortWizard
//  File Name:      RegexPatterns.cs
//  Authors:        Bobby Szwajkowski & Drew O'Brien
//  Date:           2015-02-27
//  Description:    This file contains all of the functionality for building the new C# file out of a specified 
//                  C file. It performs a series of passes over the data to first create the using statements, then
//                  it passes over and copies in C style strings after copying them

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileMan;
using System.Text.RegularExpressions;

namespace PortWizard
{
    /// <summary>
    /// The Translator class acts as a go between for converting the C style lines and writing them to a C# file.  It consists of 5 data members and 6 Methods Outlined as follows:
    /// Data Members:
    ///     ->  SyntaxSearcher SyntaxParser :   This data member handles the translation of individual lines read in from the file.
    ///     ->  int TabSpaces : This data member handles the amount of white space that should precede the translated lines depending on
    ///                         the current {} level
    ///     ->  RegexPatterns regexP : This object contains all of the hardcoded Regex Patterns necessary for parsing out the data
    ///     ->  bool FgetsDetected  :   This flag is used to tell the relevant method whether or not the C# file will need the custom fgets code
    ///     ->  bool FileStreamRequired :   This flag is used to the calling method whether or not System.IO is necessary.
    ///     
    /// Overall this class moves through the file twice. Once to build up the initial using statements, and then a second time to translate all of the pertinent details.
    /// </summary>
    class Translator
    {
        private SyntaxSearcher SyntaxParser;
        private int TabSpaces;
        private RegexPatterns regexP;
        private bool FgetsDetected;
        private bool FileStreamRequired;
        
        /// <summary>
        /// Default constructor that sets all of the necessary data members to blank/false/empty data
        /// </summary>
        public Translator()
        {
            
            SyntaxParser = new SyntaxSearcher();
            regexP = new RegexPatterns();
            TabSpaces = 0;
            FgetsDetected = false;
            FileStreamRequired = false;
        }
        
        /// <summary>
        /// This method performs the translation of the C file being handled by the
        /// FileHandler object. The method does an initial scan to first set the 
        /// using statements, then it copies in all of the converted data before it
        /// wraps up the code with the necessary }'s
        /// </summary>
        /// <param name="fileManager">The FileHandler object that contains the necessary reading and writing stream details</param>
        public void Translate(FileHandler fileManager)
        {
            String buffer;
            bool isPassedMain = false;
            bool isPassedFirstSemicolon = false;

            // Do an initial scan and prepare the file
            InitialScan(fileManager);

            
            for(int i = 0; i <= fileManager.GetNewLines(); i++)
            {
                // Parse each line of code, then write it to the new file
                // Make sure that the code starts after main though...

                buffer = SyntaxParser.parseLine(fileManager.ReadLine(i));

                if (buffer != "" && isPassedMain == true)
                {
                    TabbedWrite(fileManager, buffer);
                    if (isPassedFirstSemicolon == false && FileStreamRequired == true)
                    {
                        TabSpaces++;
                        TabbedWrite(fileManager, "UnicodeEncoding uniEncoding = new UnicodeEncoding();");
                        TabSpaces--;

                        isPassedFirstSemicolon = true;
                    }
                }
                else if(isPassedMain == false)
                {
                    // Check to see if we've reached main
                    foreach (Match m in Regex.Matches(buffer, regexP.Pattern_Main))
                    {
                        isPassedMain = true;
                        break;
                    }
                }
            }

            EndTranslation(fileManager);
        }


        /// <summary>
        /// This method scans a file for using headers before actually doing
        /// any command replacements. Uses regex to search for the commands
        /// and includes the appropriate usings.
        /// 
        /// Also generates the static void Main() header.
        /// </summary>
        /// <param name="fileManager">File manager class. Contains methods relating to reading/writing.</param>
        private void InitialScan(FileHandler fileManager)
        {
            bool SystemHeader = false;
            bool IOHeader = false;
            bool MainDetected = false;

            // Don't need to search for fopen,fclose because you need a FILE* to do any of that.
            // atoi uses String.tryparse, no headers needed.


            // Regex explanation
            // @ basically says nothing is an escape sequence
            // ^ means "at the start of the line"
            // \s*? is me saying that tabs or spaces could be in the sequence
            // The rest is pretty basic. Printf for example says there could be a space after it. There might not.
            //
            // Might need a const.h to keep track of these bloody things... >_>

            String mainType = "void";

            for (int i = 0; i <= fileManager.GetNewLines(); i++)
            {
                // Grab the whole buffer
                String buffer = fileManager.ReadLine(i);

                // Check to see if the C# file requires using System.IO and using System.Text
                if (IOHeader == false)
                {
                    foreach (Match m in Regex.Matches(buffer, regexP.Pattern_FILE))
                    {
                        Console.WriteLine(m.Value);
                        fileManager.WriteLine("using System.IO;");
                        fileManager.WriteLine("using System.Text;"); //Necessary for UniEncoding object
                        IOHeader = true;
                        break;
                    }

                    foreach (Match m in Regex.Matches(buffer, regexP.Pattern_FPrintf))
                    {
                        Console.WriteLine(m.Value);
                        fileManager.WriteLine("using System.IO;");
                        fileManager.WriteLine("using System.Text;");
                        IOHeader = true;
                        break;
                    }
                }
                //Checks to see if the C# file will need System.IO and later on if the file needs the fgets_conv code
                if(FgetsDetected == false)
                {
                    foreach (Match m in Regex.Matches(buffer, regexP.Pattern_FGets))
                    {
                        Console.WriteLine(m.Value);
                        if(IOHeader == false)
                        {
                            fileManager.WriteLine("using System.IO;");
                            fileManager.WriteLine("using System.Text;");
                            IOHeader = true;
                        }

                        FgetsDetected = true;
                        break;
                    }
                }
                //Writes in the necessary System statement is printf
                if(SystemHeader == false)
                {
                    foreach (Match m in Regex.Matches(buffer, regexP.Pattern_Printf))
                    {
                        Console.WriteLine(m.Value);
                        fileManager.WriteLine("using System;");
                        SystemHeader = true;
                        break;
                    }


                    foreach (Match m in Regex.Matches(buffer, regexP.Pattern_Gets))
                    {
                        Console.WriteLine(m.Value);
                        fileManager.WriteLine("using System;");
                        SystemHeader = true;
                        break;
                    }
                }

                //Checks to see if Main returns an int or void
                if(MainDetected == false)
                {
                    // Determine the return value of main
                    foreach (Match m in Regex.Matches(buffer, regexP.Pattern_Main))
                    {
                        if (m.Value.Substring(0, 3).ToLower() == "int")
                        {
                            mainType = "int ";
                        }
                        else
                        {
                            mainType = "void ";
                        }

                        MainDetected = true;
                        break;
                    }
                }
            }

            if(IOHeader == true)
            {
                FileStreamRequired = true;
            }

            TabbedWrite(fileManager, "namespace ported");
            TabbedWrite(fileManager, "{");

            TabSpaces += 1;

            TabbedWrite(fileManager, "class Program");
            TabbedWrite(fileManager, "{");

            TabSpaces += 1;

            TabbedWrite(fileManager, "static " + mainType + "Main(string[] args)");
        }

        /// <summary>
        /// This method wraps up the main statement. If the C# file requires the use of the custom fgets_conv method
        /// it calls upon the AddFgetsConverter method that tacks on the necessary code.
        /// </summary>
        /// <param name="fileManager">This holds the FileHandler object that controls the reading and writing stream for translation and generation</param>
        private void EndTranslation(FileHandler fileManager)
        {
            // Put in sscanf function here
            if(FgetsDetected == true)
            {
                TabbedWrite(fileManager, "");
                AddFgetsConverter(fileManager);
            }

            TabSpaces -= 1;
            TabbedWrite(fileManager, "}");
            TabSpaces -= 1;

            TabbedWrite(fileManager, "}");
            //TabSpaces = 0;
        }

        /// <summary>
        /// This method handles the number of tabbed spaces that are necessary depending on 
        /// where the code is {'s wise
        /// </summary>
        /// <param name="fileManager">This holds the FileHandler object that controls the reading and writing stream for translation and generation</param>
        /// <param name="buffer">This holds the new C# style string that is to be written to the new cs file</param>
        private void TabbedWrite(FileHandler fileManager, String buffer)
        {
            String tabs = "";

            for(int i = 0; i < TabSpaces; i++)
            {
                tabs += "   ";
            }

            fileManager.WriteLine(tabs + buffer);
        }

        /// <summary>
        /// This method writes in the fgets_conv file functionality. The fgets_conv is a 
        /// translation of the fgets method providing a read from file functionality that 
        /// stops reading at the end of the file or at a new line
        /// </summary>
        /// <param name="fileManager">This holds the FileHandler object that controls the reading and writing stream for translation and generation</param>
        private void AddFgetsConverter(FileHandler fileManager)
        {
            TabbedWrite(fileManager, "static public String fgets_conv(FileStream fs)");
            TabbedWrite(fileManager, "{");

            TabSpaces += 1;
            TabbedWrite(fileManager, "string retVal = \"\";");
            TabbedWrite(fileManager, "int buffer = 0;");
            TabbedWrite(fileManager, "while(((buffer = fs.ReadByte()) != -1) && (buffer != \'\\n\'))");
            TabbedWrite(fileManager, "{");

            TabSpaces += 1;
            TabbedWrite(fileManager, "retVal += Convert.ToChar(buffer);");

            TabSpaces -= 1;
            TabbedWrite(fileManager, "}");

            TabbedWrite(fileManager, "");
            TabbedWrite(fileManager, "if (retVal == \"\")");
            TabbedWrite(fileManager, "{");
            TabSpaces++;
            TabbedWrite(fileManager, "retVal = null;");
            TabSpaces--;
            TabbedWrite(fileManager, "}");

            TabbedWrite(fileManager, "else");
            TabbedWrite(fileManager, "{");
            TabSpaces++;
            TabbedWrite(fileManager, "retVal += \"\\n\";");
            TabSpaces--;
            TabbedWrite(fileManager, "}");

            TabbedWrite(fileManager, "return retVal;");
            TabSpaces -= 1;
            TabbedWrite(fileManager, "}");
        }
    }
}

