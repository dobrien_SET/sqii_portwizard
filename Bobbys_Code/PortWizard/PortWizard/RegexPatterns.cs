﻿//  Project Name:   SQII_PortWizard
//  File Name:      RegexPatterns.cs
//  Authors:        Bobby Szwajkowski & Drew O'Brien
//  Date:           2015-02-27
//  Description:    This file contains all of the necessary hard coded
//                  Regex search patterns used in the SQII_PortWizard project


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortWizard
{
    /// <summary>
    /// The RegexPatterns class consists of all of the necessary data members for finding specific keywords as required.
    /// As of this 2015-02-27 the file contains 15 data members that have regex patterns designed to find specific keywords.
    /// These include function calls and variable keywords as required.    
    /// </summary>
    class RegexPatterns
    {
        public String Pattern_Main = @"^(\s*?((int)|(void)) [mM]ain)";
        public String Pattern_Printf = @"^(\s*?printf ?\(.*\);$)";
        public String Pattern_FILE = @"\s*?FILE\*?\s*?.*";
        public String Pattern_FPrintf = @"^(\s*?fprintf ?\(.*\);$)";

        public String Pattern_Gets = @"^(\s*?gets ?\(.*\);$)";
        public String Pattern_FGets = @"(fgets ?\(.*\))[\s);]";
        public String Pattern_Whitespace = @"^\s*";

        public String Pattern_Fopen = "fopen\\s*?\\(\"*.*\",\\s*\".*\"\\);";
        public String Pattern_Fclose = @"^\s*?fclose\s*?\(.*\);$";
        
        public String Pattern_CharStar = @"^\s*?char\s\s*?.*\[.*?\]";
        public String Pattern_Strlen = @"strlen ?\([^)]*\)";

        public String Pattern_Atoi = @"(\s*?atoi ?\(.*\);$)";
        public String Pattern_FormatCodes = @"%\d*\.?\d*?[dfcs]";
        public String Pattern_ExtractFormatCode = @"(?<=%)\d*\.?\d+(?=[dfcs])";
        public String Pattern_NULL = @"\sNULL";
    }
}
