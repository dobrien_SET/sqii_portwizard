﻿/*
\   Project Name:   SQII_PortWizard
/   File Name:      FileHandler.cs
\   Authors:        Bobby Szwajkowski & Drew O'Brien
/   Date:           2015-02-27
\   Description:    This file contains the class definition of FileHandler, a class designed 
/                   to handle writing and reading files.
\
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace FileMan
{
    /// <summary>
    /// This class is responsible for handling fileIO. It takes in an input and
    /// output filename and manipulates these to perform a variety of actions.
    /// 
    /// The class can read in the entire contents of the file, count the number of
    /// lines in a file, write to the file in varying ways and pick out a single line
    /// from the entire contents of the file to return to a calling function.
    /// </summary>
    class FileHandler
    {
        private String InFileName;
        private String OutFileName;

        private StreamReader reader;
        private StreamWriter writer;

        int StatusCode;
        String Buffer;



        /// <summary>
        /// This constructor creates a FileHandler which reads and writes at the
        /// same file location. Only calls secondary constructor, does nothing 
        /// specific for this type of construction.
        /// </summary>
        /// <param name="fileLocation">The file the FileManager has control over.</param>
        public FileHandler(String fileLocation) : this(fileLocation, fileLocation) 
        {
            // Does nothing special
        }

        /// <summary>
        /// This is a constructor for the FileHandler class. It sets up the reading
        /// and writing streams.
        /// </summary>
        /// <param name="inputLocation">The location of the file input</param>
        /// <param name="outputLocation">The location of the file output</param>
        public FileHandler(String inputLocation, String outputLocation)
        {
            InFileName = inputLocation;
            OutFileName = outputLocation;
            StatusCode = 0;

            try
            {
                writer = File.CreateText(OutFileName);
                reader = File.OpenText(InFileName);

                // Ensure writes are performed properly
                writer.AutoFlush = true;

                try
                {
                    // Perform a writing test
                    writer.WriteLine("//Initiatiating FileHandler");
                }
                catch
                {
                    // Writing failed
                    StatusCode = 1;
                }

                try
                {
                    // Perform a reading test
                    reader.Peek();
                }
                catch
                {
                    if (StatusCode != 0)
                    {
                        // Reading and writing didn't work
                        StatusCode = 3;
                    }
                    else
                    {
                        // Reading failed. File might not exist or something else weird is going on.
                        StatusCode = 2;
                    }
                }
            }
            catch
            {
                // Reader file doesn't exist
                StatusCode = -4;
            }
        }

        /// <summary>
        /// Returns a status code indicative of whether the
        /// FileHandler has a file loaded or not. Returns 0
        /// upon success.
        /// </summary>
        /// <returns>StatusCode: 0 indicates ready to read/write, else indicates error</returns>
        public int CheckStatus()
        {
            return StatusCode;
        }

        /// <summary>
        /// Returns the meaning behind a given status code.
        /// Can be called upon CheckStatus failing to determine
        /// the cause.
        /// </summary>
        /// <returns>retVal: String containing failure reason</returns>
        public String FailReasion()
        {
            String retVal = "";

            if(StatusCode == 0)
            {
                retVal = "OK";
            }
            else if(StatusCode == 1)
            {
                retVal = "Cannot write to file";
            }
            else if(StatusCode == 2)
            {
                retVal = "Cannot read from file";
            }
            else if(StatusCode == 3)
            {
                retVal = "Cannot read or write from the provided files";
            }
            else if(StatusCode == 4)
            {
                retVal = "File to read in doesn't exist";
            }
            else
            {
                retVal = "Unknown";
            }

            return retVal;
        }

        /// <summary>
        /// Imports the contents of a file into an internal
        /// buffer.
        /// </summary>
        public void ImportFileContents()
        {
            Buffer = reader.ReadToEnd();
            reader.BaseStream.Position = 0;
        }

        /// <summary>
        /// Reads a specified line from the file. It refreshes the internal buffer with
        /// all the contents of the file and parses for the requested line.
        /// </summary>
        /// <param name="lineNum">The line to retrieve from the file.</param>
        /// <returns>retVal: A line of text from the specified line number</returns>
        public String ReadLine(int lineNum)
        {
            String retVal = "";

            if (StatusCode == 0)
            {
                // Refresh the string buffer with the latest version of the file
                ImportFileContents();


                // Split the contents of the file into newlines
                int size = Buffer.Split(new String[]{"\n","\r\n"}, StringSplitOptions.None).Length + 1;
                String[] fileData = new String[size];
                fileData = Buffer.Split(new String[]{"\r\n", "\n"}, StringSplitOptions.None);

                // Find the contents of the line
                if (lineNum < fileData.Length)
                {
                    retVal = fileData[lineNum];
                }
            }
            else
            {
                retVal = "FILE_NOT_OPEN";
            }

            return retVal;
        }

        /// <summary>
        /// Reads the entire contents of a file. It refreshes the file
        /// buffer and returns all the text to the user.
        /// </summary>
        /// <returns>retVal: All the text contained in a file.</returns>
        public String ReadAll()
        {
            String retVal = "";

            if(StatusCode == 0)
            {
                ImportFileContents();
                retVal = Buffer;
            }
            else
            {
                retVal = "FILE_NOT_OPEN";
            }

            return retVal;
        }

        /// <summary>
        /// Writes a line of text to the text file with a new line automagically appended.
        /// </summary>
        /// <param name="text">The line of text to add to the next file</param>
        /// <returns>retVal: returns true upon successful write, fail otherwise.</returns>
        public bool WriteLine(String text)
        {
            bool retVal = false;

            if(StatusCode == 0)
            {
                writer.WriteLine(text);
                retVal = true;
            }

            return retVal;
        }

        /// <summary>
        /// Writes a line of text to the text file strictly as it's provided.
        /// </summary>
        /// <param name="text">The line of text to add to the next file</param>
        /// <returns>retVal: returns true upon successful write, fail otherwise.</returns>
        public bool Write(String text)
        {
            bool retVal = false;

            if(StatusCode == 0)
            {
                writer.Write(text);
            }

            return retVal;
        }

        /// <summary>
        /// Returns a count of the number of newlines in a file.
        /// </summary>
        /// <returns>retVal: returns the number of newlines in the file; -1 if it fails.</returns>
        public int GetNewLines()
        {
            int retVal = -1;
            if(StatusCode == 0)
            {
                ImportFileContents();

                //Split the file contents into a string split by new line.
                //The length of the string array indicates how many new lines there are in the file.
                retVal = Buffer.Split(new String[] { "\n", "\r\n" }, StringSplitOptions.None).Length;
            }

            return retVal;
        }
    }
}
