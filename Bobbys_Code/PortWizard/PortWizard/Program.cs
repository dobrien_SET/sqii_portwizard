﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileMan;

namespace PortWizard
{
    class Program
    {
        static void Main(string[] args)
        {
            String inputFile = "";
            String outputFile = "";
            bool error = false;
            if(args.Length < 4 || args.Length > 4)
            {
                Console.Write("Please enter the proper format.\nIe: -i to specify input file and -o to specify output file");
            }
            else
            {
                if(args[0] == "-i")
                {
                    inputFile = args[1];
                }
                else if(args[0] == "-o")
                {
                    outputFile = args[1];
                }
                else
                {
                    error = true;
                }

                if(args[2] == "-i")
                {
                    inputFile = args[3];
                }
                else if (args[2] == "-o")
                {
                    outputFile = args[3];
                }
                else
                {
                    error = true;
                }

                if(inputFile == "" || outputFile == "")
                {
                    error = true;
                }
                


                if (error == false)
                {
                    FileHandler fileManager = new FileHandler(inputFile, outputFile);

                    Translator translate = new Translator();
                    translate.Translate(fileManager);
                    Console.Write("Conversion complete!\n");
                }
                else
                {
                    Console.Write("Please enter both an input and output file.");
                }
            }
        }
    }
}
