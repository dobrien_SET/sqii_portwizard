﻿//  Project Name:   SQII_PortWizard
//  File Name:      RegexPatterns.cs
//  Authors:        Bobby Szwajkowski & Drew O'Brien
//  Date:           2015-02-27
//  Description:    This file contains all of the necessary methods and functionality for parsing
//                  out specified keywords and function calls. A single string is passed to the
//                  class and scanned to determine if any statements are present and is then converted
//                  to a C sharp compatible statement

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace PortWizard
{
    /// <summary>
    /// The SyntaxSearcher class handles the conversion of C specific codes to their C# equivalent. It consists of a single data member and 8 methods
    /// Data Members:
    ///     ->  RegexPatterns regexP    :   This object contains all of the necessary Regex Patterns necessary for parsing out the relevant keywords
    /// The class moves through each possible keyword and upon discovering it converts that particular portion to its C# equivalent
    /// </summary>
    class SyntaxSearcher
    {
        private RegexPatterns regexP;
        
        /// <summary>
        /// Default Constructor that sets the regexP object for referencing specific regex patterns
        /// </summary>
        public SyntaxSearcher()
        {
            regexP = new RegexPatterns();
            
        }

        /// <summary>
        /// This method generates a C# style string out of a C style line. It moves through 
        /// the various series of hard coded patterns and appropriately converts them all if necessary.
        /// </summary>
        /// <param name="line">The C style line read in from the file</param>
        /// <returns>Returns the C# generated Line out of the C style line</returns>
        public String parseLine(String line)
        {
            String retVal = line;
            String buffer = "";

            int whiteSpaceLen = 0;
            int pos = 0;

            try
            {
                // Erase all the whitespace from the line. For now, at least.
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_Whitespace))
                {
                    retVal = retVal.Remove(m.Index, m.Value.Length);
                    whiteSpaceLen = m.Value.Length; // Count the white space
                }

                // Replace FILE * with FileStream
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_FILE))
                {
                    // Find the position of the * and offset it from the starting index
                    retVal = retVal.Remove(m.Index, m.Value.IndexOf('*') + 1 - m.Index);
                    retVal = retVal.Insert(m.Index, "FileStream ");
                }

                // Replace fopen with new FileStream
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_Fopen))
                {
                    // Find the open bracket for the fopen and replace it with new filestream
                    retVal = retVal.Remove(m.Index, m.Value.IndexOf('('));
                    retVal = retVal.Insert(m.Index, "new FileStream");

                    // Now we need to find HOW the file is being opened
                    pos = m.Value.IndexOf(',') + 1; // Find the location of the open mode

                    if (m.Value.Length > pos)
                    {
                        // Save the code after the fopen to a buffer
                        buffer = m.Value.Substring(pos);
                    }

                    pos = retVal.IndexOf(',') + 1; // Find the location to put the new open mode
                    retVal = retVal.Remove(pos, retVal.IndexOf(')') - pos);
                    retVal = retVal.Insert(pos, FileModeGenerator(buffer)); // Add a generated mode based on the original intentions


                }

                // Replace fclose with variable.close()
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_Fclose))
                {
                    // Find the open bracket and remove everything before it
                    retVal = retVal.Remove(m.Index, m.Value.IndexOf("(") + 1);

                    // Find the end portion so that we extract the variable name
                    retVal = retVal.Remove(retVal.LastIndexOf(")"), retVal.LastIndexOf(';') - retVal.LastIndexOf(")") + 1);
                    retVal = retVal.Insert(retVal.Length, ".Close();");
                }

                // Replace char[]s with Strings
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_CharStar))
                {
                    //Find the last space, which is before the variable name
                    pos = m.Value.LastIndexOf(' ');
                    if (pos == -1)
                    {
                        pos = m.Value.LastIndexOf('\t');
                    }

                    if (pos != -1)
                    {
                        // Remove the char before the variable name
                        retVal = retVal.Remove(m.Index, pos);

                        // Remove the []
                        retVal = retVal.Remove(retVal.IndexOf('['), retVal.IndexOf(']') - retVal.IndexOf('[') + 1);
                        retVal = retVal.Insert(m.Index, "String");
                    }
                }

                //Check for and convert printf Statements
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_Printf))
                {
                    retVal = PrintFConverter(retVal);
                }
                //Check for and convert fprintf statements
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_FPrintf))
                {
                    retVal = FPrintFConverter(retVal);
                }

                //Check for and convert atoi statements
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_Atoi))
                {
                    string atoiPart = retVal.Remove(m.Index);

                    retVal = atoiPart + " " + AtoiConverter(retVal);
                }

                //Check for and convert NULL's to C# style null
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_NULL))
                {
                    retVal = retVal.Remove(m.Index, " NULL".Length);
                    retVal = retVal.Insert(m.Index, " null");
                }

                //Check for and convert fgets to the custome fgets_converted method
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_FGets))
                {
                    
                    retVal = retVal.Remove(m.Index, m.Value.Length);
                    retVal = retVal.Insert(m.Index, FGetsConverter(m.Value));
                    
                }

                //Check for and convert gets statements
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_Gets))
                {
                    retVal = retVal.Substring(m.Value.IndexOf('(') + 1, m.Value.LastIndexOf(')') - m.Value.IndexOf('(') - 1) + " = Console.ReadLine();";
                    
                }

                //Check for and convert strlen statements
                foreach (Match m in Regex.Matches(retVal, regexP.Pattern_Strlen))
                {
                    buffer = retVal;

                    retVal = retVal.Remove(m.Index, m.Value.Length);
                    int matchlocation = m.Index;
                    int matchlocationLength = m.Value.IndexOf(')') - m.Index;

                    retVal = retVal.Insert(m.Index, buffer.Substring(m.Value.IndexOf('(') + m.Index + 1, m.Value.IndexOf(')') - m.Value.IndexOf('(') - 1) + ".Length");
                }

            }
            catch (Exception e)
            {
                retVal = "throw new Exception(\"Converter could not parse this line\");";
            }
            
            // Restore the whitespace
            for (int i = 0; i < whiteSpaceLen; i++)
            {
                retVal = retVal.Insert(0, "   ");
            }

            return retVal;
        }

        /// <summary>
        /// This method is used to determine what type of file access codes any file IO operations
        /// may need
        /// </summary>
        /// <param name="cFileMode">The C style file name that is used for specifying access to a file</param>
        /// <returns>Returns the C# equivalent of the different access operation codes</returns>
        public String FileModeGenerator(String cFileMode)
        {
            String retVal = "SYNTAX_ERROR";

            if (cFileMode.IndexOf("r+") != -1)
            {
                retVal = "FileMode.OpenOrCreate";
            }
            else if(cFileMode.IndexOf("r") != -1)
            {
                retVal = "FileMode.Open";
            }
            else if(cFileMode.IndexOf("w") != -1)
            {
                retVal = "FileMode.Create";
            }
            else if(cFileMode.IndexOf("a") != -1)
            {
                retVal = "FileMode.Append";
            }

            return retVal;
        }

        /// <summary>
        /// Converts an atoi() statement to int.Parse. 
        /// 
        /// Bug note: This is not very safe and as such we leave it to the developer to 
        ///           perform their own additional error testing. 
        /// </summary>
        /// <param name="cFileAtoi">String atoi statement</param>
        /// <returns>An int parse statement based on the contents of the atoi statement</returns>
        public string AtoiConverter(String cFileAtoi)
        {
            string retVal = "";
            int startOfFunction = cFileAtoi.IndexOf('(') + 1;
            int endOfFunction = cFileAtoi.IndexOf(')');

            string atoiContents = cFileAtoi.Substring(startOfFunction, (endOfFunction - startOfFunction));

            retVal = "int.Parse(" + atoiContents + ");";


            return retVal;
        }


        /// <summary>
        /// Converts an fprintf statement to an equivalent filestream.write statement.
        /// </summary>
        /// <param name="cFPrintF">The C style fprintf statement</param>
        /// <returns>A C# style filestream.write statement based off of the fopen statement</returns>
        public string FPrintFConverter(string cFPrintF)
        {
            string retVal = "";
            string varName = cFPrintF.Substring(cFPrintF.IndexOf('(') + 1, (cFPrintF.IndexOf(',') - cFPrintF.IndexOf('(')) - 1);
            //Extract the data in the command
            string writtenContents = cFPrintF.Substring(cFPrintF.IndexOf(',') + 1, (cFPrintF.IndexOf(')') - cFPrintF.IndexOf(',')));
            writtenContents = writtenContents.TrimEnd(new char[]{')', ';'});
            //Need to trim the last character if it is a )
            retVal = varName + ".Write(uniEncoding.GetBytes(" + writtenContents + "), 0, uniEncoding.GetByteCount(" + writtenContents + "));";

            return retVal;
        }

        /// <summary>
        /// Converts an fgets statement to the custom fgets_conv statement. This allows us to use the FileStream object for
        /// reading and writing to a file.
        /// </summary>
        /// <param name="cFgets">The C style fgets statement</param>
        /// <returns>Returns the C# fgets_conv statement</returns>
        public string FGetsConverter(string cFgets)
        {
            string retVal = "(";
                        
            retVal += cFgets.Substring(cFgets.IndexOf('(') + 1, cFgets.IndexOf(',') - (cFgets.IndexOf('(') + 1)) + " = fgets_conv(";
            retVal += cFgets.Substring(cFgets.LastIndexOf(',') + 1, cFgets.LastIndexOf(')') - (cFgets.LastIndexOf(',') + 1)) + "))";
            
            return retVal;
        }

        /// <summary>
        /// Converts a printf statement to a standardized Console.Write statement. This 
        /// includes checking for Format codes
        /// </summary>
        /// <param name="cPrintF">The C style printf statement</param>
        /// <returns>The Console.Write statement that contains the C# formated printf statement.</returns>
        public string PrintFConverter(String cPrintF)
        {
            string retVal = "Console.Write(";

            //public String Pattern_FormatCodes = @"%\d*\.?\d*?[dfcs]";
            string findConversionCodes = regexP.Pattern_FormatCodes;

            Regex checkForCodes = new Regex(findConversionCodes);
            MatchCollection totalCodes = checkForCodes.Matches(cPrintF);

            //Check to see if we have any special parameters in the string
            if (totalCodes.Count > 0)
            {
                int i = 0;
                string tempCopy = cPrintF;
                foreach (Match m in totalCodes)
                {
                    int startPoint = tempCopy.IndexOf(m.Value);
                    tempCopy = tempCopy.Remove(startPoint, m.Value.Length);
                    try
                    {
                        tempCopy = tempCopy.Insert(startPoint, CreatePlaceHolder(m.Value, i));
                    }
                    catch
                    {
                        Console.WriteLine("Error in converting {0}", m.Value);
                    }
                    i++;
                }
                retVal = retVal + tempCopy.Substring(tempCopy.IndexOf('"'));
            }
            else
            {
                //Turns out we have no special parameters, as such the data is in the format of printf("string");
                //We can use this to our advantage
                retVal = retVal + cPrintF.Substring(cPrintF.IndexOf('"'));
            }


            return retVal;
        }

        /// <summary>
        /// Converts a c style conversion code to a valid C# conversion code. Handles %c, %d, %s
        /// </summary>
        /// <param name="cStyleConversionCode">The c style conversion code that needs to be converted</param>
        /// <param name="currentIteration">The current iteration throught the conversion codes in the string</param>
        /// <returns>Returns a C# style conversion string</returns>
        public string CreatePlaceHolder(string cStyleConversionCode, int currentIteration)
        {
            string convertedCode = "";
            string checkforPadding = @"\d"; //Simple check for numbers in the character string 

            //public String Pattern_ExtractFormatCode = @"(?<=%)\d*\.?\d+(?=[dfcs])" must be in the RegexPatterns class
            string extractFormatParameters = regexP.Pattern_ExtractFormatCode; //Extracts the numeric value out of the conversion code

            Regex formatCheck = new Regex(checkforPadding);

            Match numericCheck = formatCheck.Match(cStyleConversionCode);


            if (numericCheck.Success)
            {
                //This means that the check has returned a positive on the presences of formatting codes surrounding the character flag

                //First we need to extract the numeric code
                Regex extractTheNumber = new Regex(extractFormatParameters);
                Match formatDetails = extractTheNumber.Match(cStyleConversionCode);


                if (formatDetails.Success)
                {
                    //Success, we have extracted the number, now we need to piece it together
                    string definedCode = formatDetails.Value;

                    //First check for a period
                    if (definedCode.IndexOf('.') != -1)
                    {
                        //Check to see if the code is #.# or .# format
                        if (definedCode.IndexOf('.') > 0)
                        {
                            string[] splitCode = definedCode.Split('.');
                            //Now we just need to get the correct number of zeroes to pad. This will be the number that followed the . in the C style
                            int numberOfPasses = 0;
                            string numberOfZeroes = "";
                            if (int.TryParse(splitCode[1], out numberOfPasses))
                            {
                                for (int i = 0; i < numberOfPasses; i++)
                                {
                                    numberOfZeroes = numberOfZeroes + "0";
                                }
                                //Completed sorting out the number of zeroes for the zero padding pattern, time to write out the 
                                //proper C# Console String

                                if (numberOfZeroes.Length == 0)
                                {
                                    convertedCode = "{" + currentIteration.ToString() + "," + splitCode[0] + "}";
                                }
                                else
                                {
                                    Regex checkForFloat = new Regex(@"f");
                                    if (checkForFloat.Match(cStyleConversionCode).Success)
                                    {
                                        convertedCode = "{" + currentIteration.ToString() + "," + splitCode[0] + ":0." + numberOfZeroes + "}";
                                    }
                                    else
                                    {
                                        convertedCode = "{" + currentIteration.ToString() + "," + splitCode[0] + ":" + numberOfZeroes + "}";
                                    }
                                }

                            }
                            else
                            {
                                throw new Exception("Strange error: Failed to parse out the number after the . (Style: #.#)");
                            }
                        }
                        else
                        {
                            //The format of the C style code is .# instead of #.# so we are only worried about zero padding
                            int numberOfPasses = 0;
                            string numberOfZeroes = "";
                            if (int.TryParse(definedCode.Trim('.'), out numberOfPasses))
                            {
                                for (int i = 0; i < numberOfPasses; i++)
                                {
                                    numberOfZeroes = numberOfZeroes + "0";
                                }
                                //Great! Time to build the string
                                if (numberOfZeroes.Length == 0)
                                {
                                    throw new Exception("Strange Error Again: No number after the period in the format code");
                                }
                                else
                                {
                                    Regex checkForFloat = new Regex(@"f");
                                    if (checkForFloat.Match(cStyleConversionCode).Success)
                                    {
                                        convertedCode = "{" + currentIteration.ToString() + ":0." + numberOfZeroes + "}";
                                    }
                                    else
                                    {
                                        convertedCode = "{" + currentIteration.ToString() + ":" + numberOfZeroes + "}";
                                    }
                                }
                            }
                            else
                            {
                                throw new Exception("Strange error: Failed to parse out the number after the . (Style .#)");
                            }
                        }


                    }
                    else
                    {
                        //Great! This is easy peasy, we know there is a number in front of the code so we simply add
                        //the number to the properly formated string
                        convertedCode = "{" + currentIteration.ToString() + ", " + definedCode + "}";

                    }

                }
                else
                {
                    throw new Exception("Error: Failed to parse out the format code.\nDid not match \"extractFormatParameters\" pattern");
                }

            }
            else
            {
                //There are no special format instructions, we can flag this as the basic {#} type
                convertedCode = "{" + currentIteration.ToString() + "}";
            }

            return convertedCode;
        }
    }
}
