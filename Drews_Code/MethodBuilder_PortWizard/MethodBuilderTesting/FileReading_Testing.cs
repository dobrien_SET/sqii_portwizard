﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MethodBuilder_PortWizard;

namespace MethodBuilderTesting
{
    [TestClass]
    public class FileReading_Testing
    {
        /// <summary>
        /// This file testing is used for checking to make sure that the method can read from a valid file
        /// </summary>
        [TestMethod]
        public void ValidFileOpening()
        {
            string validFileName = "E:\\Repositories\\port_wizard\\Sample_C_Files_and_Requirements\\Assignment-3-Samples\\assign3Samples\\fileIn.c";
            List<string> tempBuffer = new List<string>();

            MethodBuilder testSubject = new MethodBuilder(validFileName);

            tempBuffer = testSubject.C_FileReader();
            
            //Retrieve the number of lines in the file
            int validCount = File.ReadAllLines(validFileName).Length;
            int observedCount = tempBuffer.Count;

            Assert.AreEqual(validCount, observedCount, "Test Failed: Valid count does not equal invalid Count");


        }
    }
}
