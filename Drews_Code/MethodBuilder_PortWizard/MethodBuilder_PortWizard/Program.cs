﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MethodBuilder_PortWizard
{
    class Program
    {
        
        static void Main(string[] args)
        {


        }

                


    }

    public class MethodBuilder
    {

        //The following is a list of enum ideas that could be used as flags for certain things found within
        //the code itself

        //Starting flags to watch out for
        enum StartingComponent { PreProcessor = 0, OldCommentOpen, OldCommentClose, Comment, BlockOpen, BlockClose };

        //Starting variable keywords to watch out for
        enum VariableKeyWords { C_Int = 6, C_Void, C_Char, C_FILE, C_Define };

        //Starting flow control variables to watch for
        enum FlowControlKeywords { C_If = 11, C_For, C_While, C_Do, C_Return };

        //These are the standard functions that we know about
        enum FunctionKeywords { C_Main = 16, C_fopen, C_printf, C_fprintf, C_fclose, C_atoi, C_gets, C_fgets}

        //Simple error flag, though it should be remembered that 
        //an invalid statement may still be valid if we are in a comment block
        private const int INVALIDSTATEMENT = -1;


        private string[] STARTINGKEYWORDS = new string[] { "#", "//*", "*//", "////", "{", "}" };
        private string[] VARIABLEKEYWORDS = new string[] { "int", "void", "char", "FILE" };
        private string[] FLOWCONTROLKEYWORDS = new string[] { "if", "for", "while", "do", "return" };
        private string[] FUNCTIONKEYWORDS = new string[] { "main", "fopen", "printf", "fprintf", "fclose", "atoi", "gets", "fgets" };

        private bool inQuotes;
        private bool inComments;

        private string cFileName;

        private List<string> cSharpText;


        /// <summary>
        /// Basic default constructor. Sets all private variables to blank values or false
        /// </summary>
        public MethodBuilder()
        {
            inQuotes = false;
            inComments = false;
            cFileName = "";
        }

        /// <summary>
        /// Overridden constructor that takes in a file name to work with. For now it doesn't check to see if it is valid or not
        /// </summary>
        /// <param name="newFileName">The c file that we want to open and work with</param>
        public MethodBuilder(string newFileName)
        {
            inQuotes = false;
            inComments = false;
            cFileName = newFileName;
        }




        /// <summary>
        /// A simple method that reads through a given file and puts each line of text into a List that will 
        /// hold all of the strings found within the file.
        /// </summary>
        /// <returns>Returns a list that contains all of the file contents</string></returns>
        public List<string> C_FileReader()
        {
            List<string> fileContents = new List<string>();
            StreamReader readerHandle;
            string tempString = "";

            //Try to open the file for reading
            try
            {
                readerHandle = new StreamReader(CFileName);
            }
            catch (Exception e)
            {
                throw e;
            }

           
            //Progress through until we reach the end of the file
            while ((tempString = readerHandle.ReadLine()) != null)
            {
                fileContents.Add(tempString);
            }

            return fileContents;
        }



        /// <summary>
        /// This simple method is used to sort out what type of statement the user is 
        /// dealing with. It goes through a set of particular statements and returns
        /// a number code that corresponds with an enum
        /// </summary>
        /// <param name="statement">The string that the method will be working with</param>
        /// <returns>Returns a known flag value that is used to figure out what to do next</returns>
        public int GetStatementType(string statement)
        {
            int statementFlag = 0;
            char[] firstWord = new char[statement.Length];

            //This can be a constant 
            char[] openingWord = { ' ', '(', '{' };
            int wordEndsAt = statement.IndexOfAny(openingWord);
            //We need to get the firstword in the statement
            //Check for first occurance of spaces or open braces(
            if (wordEndsAt > 0)
            {
                //Set up the first word and the first character
                statement.CopyTo(0, firstWord, 0, wordEndsAt);

                //Now we need to check for what was returned
                string testFirstWord = firstWord.ToString();

                //The variable keywords we need to watch out for are as follows
                //int, void, char, FILE

                for (int i = 0; i < STARTINGKEYWORDS.Length; i++)
                {
                    //Check to see if it is a valid variable word
                    if (STARTINGKEYWORDS[i].Equals(testFirstWord))
                    {
                        statementFlag = i;
                    }

                }

            }
            else
            {
                statementFlag = INVALIDSTATEMENT;
            }

            //Return the flag for what we have found
            return statementFlag;
        }

        public void StartConverting()
        {
            List<string> cFileContents = new List<string>();

            //First we start reading in the file
            try
            {
                cFileContents = C_FileReader();
            }
            catch
            {
                Console.WriteLine("Error, detected. Something is wrong with the WriteFile");
            }

            //First thing we need to do is see what the file starts with any header comments
            
            //Then maybe we can move through the whole file and see what functions are present
            //Once we know that we can build up the using statements
            //However I think this will require some design tweaking and playing around to sort it all out


        }





        #region Accessors & Mutators

        /// <summary>
        /// Basic accessor/mutator for the inQuotes variable. This flag is used to check to see if any keywords were found inside quotes (in which case we should ignore them)
        /// </summary>
        public bool InQuotes
        {
            get
            {
                return inQuotes;
            }
            set
            {
                inQuotes = value;
            }
        }


        /// <summary>
        /// Basic accessor/mutator for the inComments variable. This flag is used to check to see if the text is in comments (in which case we ignore all keywords)
        /// </summary>
        public bool InComments
        {
            get
            {
                return inComments;
            }
            set
            {
                inComments = value;
            }

        }

        /// <summary>
        /// Basic accessor/mutator for the cFileName variable. This data holds the file name throughout the session so we don't have to worry about passing it down 
        /// through the methods.
        /// </summary>
        public string CFileName
        {
            get
            {
                return cFileName;
            }
            set
            {
                cFileName = value;
            }
        }


        /// <summary>
        /// Basic accessor/mutator for the cSharpText variable. This data holds the new lines of text that will be used to create the C# equivalent of the C Program
        /// </summary>
        public List<string> CSharpText
        {
            get
            {
                return cSharpText;
            }
            set
            {
                cSharpText = value;
            }
        }

        #endregion

    }
   
}
