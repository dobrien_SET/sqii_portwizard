﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions; //Necessary for setting up the Regular Expressions
using System.Threading.Tasks;

namespace RegexPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            string printfTest = "printf(\"Hello World\n\");";
            string weirdTest = "printf(\"Oooohh Here's another printf()\"); ";
            string conversioncodeTest = "printf(\"%d, %2d, %2.1d\", 5, 5, 10)";
            string reallyWeirdTest = "printf(\"Ooo dang, here's\n another newline%s\n\", \"derp\n\")";

            string knownTest = "printf(\"%d normal Number\n%3d extra Number\n%5.3d decimal Number\n\", 7, 7, 7)";


            Console.WriteLine("Converting: {0}", printfTest);
            Console.WriteLine("Result of Conversion: {0}", BuildWriteStatement(printfTest, false));
            Console.WriteLine("");
            Console.WriteLine("Converting: {0}", conversioncodeTest);
            Console.WriteLine("Result of Conversion: {0}", BuildWriteStatement(conversioncodeTest, true));
            Console.WriteLine("");
            Console.WriteLine("COnverting: {0}", reallyWeirdTest);
            Console.WriteLine("Result of Conversion: {0}", BuildWriteStatement(reallyWeirdTest, true));
            Console.ReadLine();

            //http://regexone.com/lesson/0

            
            string escapecharacterPattern = @"%\d*[d]";
            string oneParameterNewLine = @"\\n""(?=\))";
            string multiParameterNewLine = @"\\n""(?=,)";
            string superEscapeCharacterPattern = @"%\d*\.?\d*?[dfcs]";
            string extractEscapeCharacterNumbers = @"(?<=%)\d*\.?\d(?=[dfcs])";

            

            Regex QuoteChecker = new Regex(extractEscapeCharacterNumbers);
            MatchCollection conversionCaracters = QuoteChecker.Matches(knownTest);
            int i = 0;
            foreach (Match code in conversionCaracters)
            {

                Console.WriteLine("Match found {0}", code.Value);
                Console.WriteLine("String Returned = {0}", ConversionFlag(code.Value, i));
                i++;

            }
            
            int x = 45;
            int table = 97;
            int product = 800;
            Console.WriteLine("{0, 3} extra Number\n{1, 5:000} Zero Padded number", 7, 7);
            Console.Write("{0:00.00}, {1,15:00.00}, {2,15:0.00000}", x, table, product);
            Console.ReadLine();
            

            
        }


        //Return Conversion status 
        //This method only works with the numeric portion of the conversion code
        static public string ConversionFlag(string extractedPattern, int currentPosition)
        {
            //string cSharpStyleFormat = "{" + currentPosition.ToString() + ":" + extractedPattern + "}";
            string cSharpStyleFormat = "";
            int firstNumber = 0;
            int secondNumber = 0;


            //Look for decimal places in the conversion code
            if(extractedPattern.IndexOf('.') != -1)
            {
                string[] numberPieces = extractedPattern.Split('.');
                if((int.TryParse(numberPieces[0], out firstNumber)) && (int.TryParse(numberPieces[1], out secondNumber)))
                {
                    string numberOfZeroes = "";
                    for (int i = 0; i < secondNumber; i++)
                    {
                        numberOfZeroes = numberOfZeroes + "0";
                    }
                    if(numberOfZeroes.Length == 0)
                    {
                        cSharpStyleFormat = "{" + currentPosition.ToString() + "," + firstNumber.ToString() + "}";
                    }
                    else
                    {
                        cSharpStyleFormat = "{" + currentPosition.ToString() + "," + firstNumber.ToString() + ":" + numberOfZeroes + "}";
                    }
                        
                }
                else
                {
                    Console.WriteLine("Error parsing the numberPieces that had a . in them");
                }
            }
            else
            {
                try
                {
                    firstNumber = int.Parse(extractedPattern);
                                      
                    cSharpStyleFormat = cSharpStyleFormat + "{" + currentPosition.ToString() + "," + firstNumber + "}"; 
                }
                catch
                {
                    Console.WriteLine("Failure To parse in single number example");
                }
            }
                      
            return cSharpStyleFormat;
        }

        static public bool IsStatementMultiParameter(string stringToTest)
        {
            bool isMultiParameter = false;
            string quoteCheckPattern = @"\(?[^\\]""\)?";

            Regex quoteChecker = new Regex(quoteCheckPattern);
            MatchCollection numberOfQuotes = quoteChecker.Matches(stringToTest);
            if(numberOfQuotes.Count > 2)
            {
                isMultiParameter = true;
            }
            
            return isMultiParameter;
        }

        static public string BuildWriteStatement(string stringToTest, bool isMultiParameter)
        {
            string writeStatement = "";
            string startingPointNoNewLine = "Console.Write(";
            string startingPointNewLine = "Console.WriteLine(";
            string newLinePattern = "";
            string[] testStringBreakdown = stringToTest.Split('"');
            string consoleContents = "";
            string seperateParameterContents = @"(?<="")[^""]*(?="")";
            //String has multiple parameters
            //Need to account for whether there is more than one parameter or not
            if(isMultiParameter)
            {
                newLinePattern = @"\\n""(?=,)";
                int currentLocationInString = 0;

                //We may need to extract more than one quoted value
                //Note this does not handle escaped sequences for quotation marks in the string
                Regex stringContent = new Regex(seperateParameterContents);

                MatchCollection collectComponents = stringContent.Matches(stringToTest);
                
                // Means there is only one string value
                if(collectComponents.Count >= 1)
                {
                    //So we need to start moving through the first collection and begin extracting all of the
                    //conversion codes
                    string extractEscapeCharacterNumbers = @"(?<=%)\d*\.?\d(?=[dfcs])";
                    consoleContents = consoleContents + "\"";
                    Regex QuoteChecker = new Regex(extractEscapeCharacterNumbers);
                    MatchCollection conversionCaracters = QuoteChecker.Matches(collectComponents[0].Value);
                    int i = 0;
                    foreach (Match code in conversionCaracters)
                    {

                        consoleContents = consoleContents + collectComponents[0].Value.Substring(currentLocationInString, collectComponents[0].Value.IndexOf(code.Value)) + (ConversionFlag(code.Value, i));
                        
                        //Move to next parameter code
                        i++;
                        //Move start of currentLocationInString
                        currentLocationInString = collectComponents[0].Value.IndexOf(code.Value) + code.Value.Length;

                    }

                    if (collectComponents.Count == 1)
                    {
                        consoleContents = consoleContents + "\", " + stringToTest.Substring(stringToTest.LastIndexOf('"'));
                    }
                    else
                    {
                        //This means that the contents of the printf statement consist of several Quotation parameters so we need to seperate them out 
                        //Part of the problem here is dealing with escape quotations. The following regex returns all of the data after the first comma
                        string extractParameters = @"(?=,).*(?=\))";
                        Regex ParameterChecker = new Regex(extractParameters);
                        Match extractedData = ParameterChecker.Match(stringToTest);

                        if(extractedData.Success)
                        {
                            consoleContents = consoleContents + "\", " + extractedData.Value;
                        }
                        else
                        {
                            Console.WriteLine("Failed to extract the parameters properly");
                        }


                    }

                }
                else //Some sort of error has occured here
                {
                    Console.WriteLine("Placeholder for proper error message");
                }


                //The content of the console.wrte/writeline has been built and only needs the ending wrapper
                consoleContents = consoleContents + ");";

            }
            else
            {
                newLinePattern = @"\\n""(?=\))";
                //@"(?<="").*(?="")" literal for testing
                Regex stringContent = new Regex(seperateParameterContents); //Extracts a single quoted string
                //Okay so since we know that the printf is a single parameter we can simply extract the data in the quotation marks and move along
                Match extractString = stringContent.Match(stringToTest);
                if(extractString.Success)
                {
                    consoleContents = "\"" + extractString.Value + "\");"; 
                }

            }

            Regex newLineTest = new Regex(newLinePattern);
            if (newLineTest.IsMatch(stringToTest))
            {
                writeStatement = startingPointNewLine;
            }
            else
            {
                writeStatement = startingPointNoNewLine;
            }

            writeStatement = writeStatement + consoleContents;


            return writeStatement;
        }
    }
}
