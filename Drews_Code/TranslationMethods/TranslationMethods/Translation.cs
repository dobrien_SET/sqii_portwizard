﻿//  Project Name:   PortWizard
//  File Name:      Translation.cs
//  Date:           2015-02-23      ->  File first created
//  
//  Description:    This file contains all of the necessary translation methods used to sort out what keywords there are and 
//                  how they should/shouldn't be manipulated
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TranslationMethods
{
    public class Translation
    {

        private bool inComments;

        private string[] KEYWORDS = { "int, void, char, FILE, if, for, while, do, return, main, fopen, printf, fprintf, fclose, atoi, gets, fgets" };
        



        /// <summary>
        /// Default blank constructor. Not used for anthing special.
        /// </summary>
        public Translation()
        {
            //Populate any data members if necessary
            inComments = false;
        }


        public List<string> BuildUsingStatements(List<string> fileContents)
        {
            List<string> usingStatements = new List<string>();
            //Now we need a list of important keywords to work with
            int i = 0;
            int j = 0;

            string[] fileFunctionKeywords = { "fopen, fclose, FILE" };
            string[] includeKeywords = { "stdio.h", "stdlib.h", "string.h" };

            //First we go through the entire list to sort out the include keywords

            while (i < fileContents.Count)
            {
                while(j < includeKeywords.Length)
                {
                    if(fileContents[i].Contains(includeKeywords[j]))
                    {
                        if(ValidateKeyWord(fileContents[i], includeKeywords[j]))
                        {
                            //Great! That keyword is considered valid
                            usingStatements.Add("using System;");
                            usingStatements.Add("using System.Console;");

                            //Break out by pushing the variables to the end
                            i = fileContents.Count - 1;
                            j = includeKeywords.Length - 1;

                        }
                    }

                    j++;
                }//End of while loop checking the individual string for the include file
                
                j = 0;
                i++;
            }//End of while loop checking the file contents for header files

            //Next we check to see if file IO is required
            i = 0;
            j = 0;

            while(i < fileContents.Count)
            {
                while(j < fileFunctionKeywords.Length)
                {
                    if(fileContents[i].Contains(fileFunctionKeywords[j]))
                    {
                        if(ValidateKeyWord(fileContents[i], fileFunctionKeywords[j]))
                        {
                            //Okay! We know that one of the file io keywords is present
                            //Time to include the using statement
                            usingStatements.Add("using System.IO;");

                            i = fileContents.Count - 1;
                            j = includeKeywords.Length - 1;
                        }
                    }

                    j++;
                }//End of while statement searching the individual string

                
                j = 0;
                i++;
            }
            




            return usingStatements;
        }

        


        /// <summary>
        /// Checks to see if the keyword is valid in the string or not.  If it is surrounded by quotation marks 
        /// or is in a comment, then it is safe to assume that the keyword is not valid and to be ignored
        /// </summary>
        /// <param name="keywordString">The entire string that contains the keyword of interest</param>
        /// <param name="keywordOfInterest">The keyword of interest that we have found</param>
        /// <returns>A flag that says whether the keyword that was found was considered valid or not</returns>
        public bool ValidateKeyWord(string keywordString, string keywordOfInterest)
        {
            bool keywordValid = false;

                        
            //Check to make sure we aren't in a commented section
            if(!InComments) 
            {
                //Check to make sure the keyword isn't wrapped in comments or single line comments
                
                if((keywordString.IndexOf('"') < keywordString.IndexOf(keywordOfInterest)) && (keywordString.IndexOf("//") < keywordString.IndexOf(keywordOfInterest)))
                {
                    keywordValid = true;
                }

            }

            
              
            return keywordValid;
        }

        public void CheckForOldComments(string keywordString)
        {
            if((keywordString.IndexOf("/*") != -1) && (InComments == false))
            {
                if (!IsInQuotes(keywordString, "/*"))
                {
                    InComments = true;
                }
            }
            else if ((keywordString.IndexOf("*/") != -1) && (InComments == true))
            {
                if (!IsInQuotes(keywordString, "*/"))
                {
                    InComments = false;
                }
            }

        }
        
        /// <summary>
        /// Checks to see if a certain keyword in a string is surrounded by quotes at any point.
        /// </summary>
        /// <param name="keywordString">String that contains the keyword</param>
        /// <param name="wordOfInterest">keyword of interest in the string</param>
        /// <returns>Returns true if the string is in a quoted portion</returns>
        public bool IsInQuotes(string keywordString, string wordOfInterest)
        {
            bool stringIsInQuotes = false;

            //Good example of Regex string that may be useful here
            //http://stackoverflow.com/questions/11620250/regex-match-keywords-that-are-not-in-quotes
            //For now I'm just going to leave this clunky bit in and move on to the next portion of
            //the translation process
            if ((keywordString.IndexOf('"') != -1) && (keywordString.IndexOf('"') < keywordString.IndexOf(wordOfInterest)))
            {
                stringIsInQuotes = true;
            }

            return stringIsInQuotes;
        }


        


        #region Accessors and Mutators

        /// <summary>
        /// Basic accessor/mutator for the inComments data member. This data member is used to tell the program whether the
        /// line is currently in a comment block or not.
        /// </summary>
        public bool InComments
        {
            get
            {
                return inComments;
            }
            set
            {
                inComments = value;
            }
        }

        #endregion


    }
}
