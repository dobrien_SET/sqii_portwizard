﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FunctionReplacer
{
    static public class PrintfConverter
    {

        static private string CONSOLEWRITE = "Console.Write(";
        static private string CONSOLEWRITELINE = "Console.WriteLine(";


        /// <summary>
        /// Converts a confirmed printf() statement to equivalent Console.Write/Write.Line code
        /// </summary>
        /// <param name="fullStatement">The entire printf statement, This could be printf("hello world");</param>
        /// <returns>returns the console style string</returns>
        static public string PrintfToConsole(string fullStatement)
        {
            string consoleStatement = "";
            




            return consoleStatement;
        }


        /// <summary>
        /// Converts a c style conversion code to a valid C# conversion code. Handles %c, %d, %s
        /// </summary>
        /// <param name="cStyleConversionCode">The c style conversion code that needs to be converted</param>
        /// <param name="currentIteration">The current iteration throught the conversion codes in the string</param>
        /// <returns>Returns a C# style conversion string</returns>
        static public string CreatePlaceHolder(string cStyleConversionCode, int currentIteration)
        {
            string convertedCode = "";
            string checkforPadding = @"\d"; //Simple check for numbers in the character string 
            string extractFormatParameters = @"(?<=%)\d*\.?\d+(?=[dfcs])"; //Extracts the numeric value out of the conversion code

            Regex formatCheck = new Regex(checkforPadding);

            Match numericCheck = formatCheck.Match(cStyleConversionCode);


            if (numericCheck.Success)
            {
                //This means that the check has returned a positive on the presences of formatting codes surrounding the character flag

                //First we need to extract the numeric code
                Regex extractTheNumber = new Regex(extractFormatParameters);
                Match formatDetails = extractTheNumber.Match(cStyleConversionCode);


                if(formatDetails.Success)
                {
                    //Success, we have extracted the number, now we need to piece it together
                    string definedCode = formatDetails.Value;
                    
                    //First check for a period
                    if(definedCode.IndexOf('.') != -1)
                    {
                        //Check to see if the code is #.# or .# format
                        if(definedCode.IndexOf('.') > 0)
                        {
                            string[] splitCode = definedCode.Split('.');
                            //Now we just need to get the correct number of zeroes to pad. This will be the number that followed the . in the C style
                            int numberOfPasses = 0;
                            string numberOfZeroes = "";
                            if(int.TryParse(splitCode[1], out numberOfPasses))
                            {
                                for (int i = 0; i < numberOfPasses; i++)
                                {
                                    numberOfZeroes = numberOfZeroes + "0";
                                }
                                //Completed sorting out the number of zeroes for the zero padding pattern, time to write out the 
                                //proper C# Console String
                                if (numberOfZeroes.Length == 0)
                                {
                                    convertedCode = "{" + currentIteration.ToString() + "," + splitCode[0] + "}";
                                }
                                else
                                {
                                    convertedCode = "{" + currentIteration.ToString() + "," + splitCode[0] + ":" + numberOfZeroes + "}";
                                }   
                                
                            }
                            else
                            {
                                throw new Exception("Strange error: Failed to parse out the number after the . (Style: #.#)");
                            }
                        }
                        else
                        {
                            //The format of the C style code is .# instead of #.# so we are only worried about zero padding
                            int numberOfPasses = 0;
                            string numberOfZeroes = "";
                            if(int.TryParse(definedCode.Trim('.'), out numberOfPasses))
                            {
                                for (int i = 0; i < numberOfPasses; i++)
                                {
                                    numberOfZeroes = numberOfZeroes + "0";
                                }
                                //Great! Time to build the string
                                if (numberOfZeroes.Length == 0)
                                {
                                    throw new Exception("Strange Error Again: No number after the period in the format code");
                                }
                                else
                                {
                                    convertedCode = "{" + currentIteration.ToString() + ":" + numberOfZeroes + "}";
                                }                                                         
                            }
                            else
                            {
                                throw new Exception("Strange error: Failed to parse out the number after the . (Style .#)");
                            }
                        }


                    }
                    else
                    {
                        //Great! This is easy peasy, we know there is a number in front of the code so we simply add
                        //the number to the properly formated string
                        convertedCode = "{" + currentIteration.ToString() + ", " + definedCode + "}";
                        
                    }
                    
                    

                }
                else
                {
                    throw new Exception("Error: Failed to parse out the format code.\nDid not match \"extractFormatParameters\" pattern");
                }

            }
            else
            {
                //There are no special format instructions, we can flag this as the basic {#} type
                convertedCode = "{" + currentIteration.ToString() + "}";
            }



            return convertedCode;
        }

        /// <summary>
        /// Check to see if the statement has more than one parameter. 
        /// </summary>
        /// <param name="statementToTest">The entire statement to test</param>
        /// <returns>Returns a true or false flag that tells the calling method whether there are multiple parameters or not</returns>
        static public bool IsStatementMultiParameter(string statementToTest)
        {
            bool isMultiParameter = false;
            string quoteCheckPattern = @"\(?[^\\]""\)?";

            Regex quoteChecker = new Regex(quoteCheckPattern);
            MatchCollection numberOfQuotes = quoteChecker.Matches(statementToTest);

            if((numberOfQuotes.Count > 2) || ((numberOfQuotes[numberOfQuotes.Count - 1].Value.IndexOf(')') == -1)))
            {
                isMultiParameter = true;
            }


            return isMultiParameter;
        }



    }
}
